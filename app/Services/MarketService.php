<?php 

namespace App\Services;
use App\Traits\AuthorizesMarketRequest;
use App\Traits\ConsumesExternalServices;
use App\Traits\InteractsWithMarketResponses;

//CARACTERIZARA LA MANERA DE REALIZAR OPERACIONES CONTRA LA API
class MarketService
{
    use ConsumesExternalServices, AuthorizesMarketRequest, InteractsWithMarketResponses;

    //REPRESENTA LA URL BASE A UTILIZAR
    protected $baseUri;

    public function __construct(){
        $this->baseUri = config('services.market.base_uri');
    }

    public function getProducts(){
        return $this->makeRequest('GET', 'products');
    }

    public function getProduct($id){
        return $this->makeRequest('GET', "products/{$id}");
    }

    public function getCategories(){
        return $this->makeRequest('GET', 'categories');
    }

    public function getCategoryProducts($id){
        return $this->makeRequest('GET', "categories/{$id}/products");
    }
}
