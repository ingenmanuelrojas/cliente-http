<?php 

namespace App\Traits;
use App\Services\MarketAuthenticationService;

trait AuthorizesMarketRequest{
        //EL & SE USA PARA INDICAR QUE SON VARIABLES PASADAS POR REFERENCIA
    public function resolveAuthorization(&$queryParams, &$formParams, &$headers){
        //llamar al metodo para acceder a un access token valido
        $accessToken = $this->resolveAccessToken();

        //Agregar el token a la cabecera authorization
        $headers['authorization'] = $accessToken;
    }

    public function resolveAccessToken(){
        //resolve() permite llamar y resolver una clase externa
        $authenticationService = resolve(MarketAuthenticationService::class);
    
        return $authenticationService->getClientCredentialsToken();
    }
}
