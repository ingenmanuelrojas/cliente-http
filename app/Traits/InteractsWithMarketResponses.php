<?php 

namespace App\Traits;

trait InteractsWithMarketResponses{
    //SE ENCARGA DE CODIFICAR LA RESPUESTA
    public function decodeResponse($response){
        $decodedResponse = json_decode($response);

        if (isset($decodedResponse->data)) {
            return $decodedResponse->data;
        }else{
            return $decodedResponse;
        }

    }

    //VALIDA SI LA RESPUESTA TIENE UN ERROR
    public function checkIfErrorResponse($response){
        if (isset($response->error)) {
            throw new \Exception("Something failed: {$response->error}");
        }
    }
}
