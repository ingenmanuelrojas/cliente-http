<?php 

namespace App\Traits;
use GuzzleHttp\Client;

trait ConsumesExternalServices{


    //ENVIA UN REQUEST A UN SERVICIO
    //RETORNA UNA RESPUESTA DE LA PETICION
    public function makeRequest($method, $requestUrl, $queryParams = [], $formParams = [], $headers = []){
        

        $client = new Client([
            'base_uri' => $this->baseUri,
        ]);
        

        //VALIDAR LA AUTENTICACION
        if (method_exists($this, 'resolveAuthorization')) {
            $this->resolveAuthorization($queryParams, $formParams, $headers);
        }
        //REALIZAR EL LLAMADO AL SERVICIO
        $response = $client->request($method, $requestUrl, [
            'query'      => $queryParams,
            'formParams' => $formParams,
            'headers'    => $headers,
        ]);

        //OBTENER EL CUERPO DEL RESPONSE
        $response = $response->getBody()->getContents();
        //VALIDAR EL TIPO DE SALIDA DEL RESPONSE
        if (method_exists($this, 'decodeResponse')) {
            $response = $this->decodeResponse($response);
        }

        
        //VALIDAR SI VIENE ALGUN ERROR EN EL RESPONSE
        if (method_exists($this, 'checkIfErrorResponse')) {
            $this->checkIfErrorResponse($response);
        }

        return $response;
    }

}
